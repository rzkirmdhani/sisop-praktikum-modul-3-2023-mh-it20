#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <time.h>


void logMessage(char *type, char *message) {
    time_t current_time;
    struct tm *local_time;

    time(&current_time);
    local_time = localtime(&current_time);

    printf("[%02d/%02d/%02d %02d:%02d:%02d] [%s] %s\n",
           local_time->tm_mday, local_time->tm_mon + 1, local_time->tm_year % 100,
           local_time->tm_hour, local_time->tm_min, local_time->tm_sec,
           type, message);
}



int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <-kata|-huruf>\n", argv[0]);
        return 1;
    }

    char *option = argv[1];
    int isWordCount = strcmp(option, "-kata") == 0;

    int fd[2];
    if (pipe(fd) == -1) {
        perror("Pipe Failed");
        return 1;
    }

    pid_t p = fork();

    if (p < 0) {
        perror("fork Failed");
        return 1;
    }

    if (p > 0) {
        close(fd[0]); // Close reading end of the pipe



        char input_str[100];
        printf("Enter lirik lagu: ");
        fgets(input_str, sizeof(input_str), stdin);

        write(fd[1], input_str, strlen(input_str) + 1);
        close(fd[1]);

        wait(NULL);

        close(fd[0]); // Close reading end of the pipe
    } else {
        close(fd[1]); // Close writing end of the pipe

        char input_str[100];
        read(fd[0], input_str, sizeof(input_str));
        close(fd[0]);


        if (isWordCount) {
            // Count word frequencies
            char result_str[100];
            char *word = strtok(input_str, " \t\n\r");
            int count = 0;

            while (word != NULL) {
                if (strcmp(word, "di") == 0) {
                    count++;
                }
                word = strtok(NULL, " \t\n\r");
            }

            snprintf(result_str, sizeof(result_str), "Kata 'di' muncul sebanyak %d kali dalam lirik lagu", count);
            logMessage("KATA", result_str);
        } else {
            // Count character frequencies (case sensitive)
            char result_str[100];
            char *character = input_str;
            int count = 0;



            while (*character != '\0') {
                if (*character == 'k') {
                    count++;
                }
                character++;
            }

            snprintf(result_str, sizeof(result_str), "Huruf 'k' muncul sebanyak %d kali dalam lirik lagu", count);
            logMessage("HURUF", result_str);
        }

        close(fd[1]); // Close writing end of the pipe
        exit(0);
    }

    return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/time.h>
#include <sys/types.h>
#include <pthread.h>

#define baris 2
#define kolom 2

void transposeMatrix(int matrix[baris][kolom], int transposed[kolom][baris]) {
    for (int i = 0; i < baris; i++) {
        for (int j = 0; j < kolom; j++) {
            transposed[j][i] = matrix[i][j];
        }
    }
}

unsigned long long factorial(int n) {
    if (n <= 1) {
        return 1;
    } else {
        unsigned long long result = 1;
        for (int i = 2; i <= n; i++) {
            result *= i;
        }
        return result;
    }
}

typedef struct {
    int row;
    int col;
} MatrixIndex;

void* kalkuFactorial(void* arg) {
    MatrixIndex* indeks = (MatrixIndex*)arg;
    int row = indeks->row;
    int col = indeks->col;

    int matrix[kolom][baris];
    key_t key = 5678;

    int shmid = shmget(key, sizeof(int[baris][kolom]), 0666);
    int (*sharedMatrix)[kolom] = shmat(shmid, (void*)0, 0);

    int transposedMatrix[kolom][baris];
    transposeMatrix(sharedMatrix, transposedMatrix);

    unsigned long long result = factorial(transposedMatrix[col][row]);

    shmdt(sharedMatrix);

    unsigned long long* result_ptr = (unsigned long long*)malloc(sizeof(unsigned long long));
    *result_ptr = result;

    return result_ptr;
}

int main() {
    key_t key = 5678;

    int shmid = shmget(key, sizeof(int[baris][kolom]), 0666);

    int (*sharedMatrix)[kolom] = shmat(shmid, (void*)0, 0);

    int transposedMatrix[kolom][baris];
    transposeMatrix(sharedMatrix, transposedMatrix);

    struct timeval start, end;
    gettimeofday(&start, NULL);

    printf("Transposed Matrix:\n");
    for (int i = 0; i < kolom; i++) {
        for (int j = 0; j < baris; j++) {
            printf("%12d", transposedMatrix[i][j]);
        }
        printf("\n");
    }

    pthread_t threads[baris][kolom];
    MatrixIndex indeks[baris][kolom];

    for (int i = 0; i < baris; i++) {
        for (int j = 0; j < kolom; j++) {
            indeks[i][j].row = i;
            indeks[i][j].col = j;

            pthread_create(&threads[i][j], NULL, kalkuFactorial, &indeks[i][j]);
        }
    }

    unsigned long long factorialMatrix[baris][kolom];
    for (int i = 0; i < baris; i++) {
        for (int j = 0; j < kolom; j++) {
            unsigned long long* result;
            pthread_join(threads[i][j], (void**)&result);
            factorialMatrix[j][i] = *result;
            free(result);
        }
    }

     gettimeofday(&end, NULL);
     double totalTime = (end.tv_sec - start.tv_sec) + (end.tv_usec - start.tv_usec) / 1000000.0;

    printf("Factorial Matrix:\n");
    for (int i = 0; i < baris; i++) {
        for (int j = 0; j < baris; j++) {
            printf("%25llu", factorialMatrix[i][j]);
        }
        printf("\n");
    }

    printf("Total Time: %f sec\n", totalTime);

    shmdt(sharedMatrix);

    return 0;
}
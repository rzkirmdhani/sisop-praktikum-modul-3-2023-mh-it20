#include <stdio.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#define PORT 8080

int checkUsr(char user[100])
{
    char db[100] = "user.txt";
    int flag = 1;
    FILE *file = fopen(db, "r");

    if (!file)
    {
        flag = 2;
    }

    char line[1000];

    while (fgets(line, sizeof(line), file))
    {
        const char sep[2] = ":";
        char *token;

        token = strtok(line, sep);
        if (strcmp(token, user) == 0)
        {
            flag = -1;
        }
    }

    fclose(file);

    return flag;
}

int checkPw(char pass[100])
{
    int flag = 1;
    int low = 0;
    int up = 0;
    int num = 0;
    int len = strlen(pass);
    for (int i = 0; i < len; i++)
    {
        if (isupper(pass[i]))
        {
            up++;
        }
        if (islower(pass[i]))
        {
            low++;
        }
        if (isdigit(pass[i]))
        {
            num++;
        }
    }
    if (up == 0 | low == 0 | num == 0 | len < 6)
    {
        flag = -1;
    }

    return flag;
}

void reg(char user[100], char pass[100])
{
    char db[100] = "user.txt";
    FILE *file = fopen(db, "a");

    char content[100];
    strcpy(content, user);
    strcat(content, ":");
    strcat(content, pass);
    strcat(content, "\n");

    fprintf(file, content);
    fclose(file);
}

int login(char user[100], char pass[100])
{
    int find = -1;
    char search[100];
    strcpy(search, user);
    strcat(search, ":");
    strcat(search, pass);
    strcat(search, "\n");
    char db[100] = "user.txt";
    FILE *file = fopen(db, "r");

    char line[1000];

    while (fgets(line, sizeof(line), file))
    {
        if (strcmp(search, line) == 0)
        {
            find = 1;
            break;
        }
    }

    fclose(file);

    return find;
}

void addTitle(char title[100], char user[100])
{
    char db[100] = "problem.tsv";
    FILE *file = fopen(db, "a");

    char content[100];
    strcpy(content, title);
    strcat(content, "\t");
    strcat(content, user);
    strcat(content, "\n");

    fprintf(file, content);
    fclose(file);
}

void makeDir(char path[100])
{
    mkdir(path, S_IRWXU);
}

void makeFile(char path[100], char dest[100])
{
    char ch;

    FILE *source, *target;

    source = fopen(path, "r");

    if (source == NULL)
    {
        printf("1");
        exit(EXIT_FAILURE);
    }

    target = fopen(dest, "w");

    if (target == NULL)
    {
        fclose(source);
        exit(EXIT_FAILURE);
    }

    while ((ch = fgetc(source)) != EOF)
        fputc(ch, target);

    fclose(source);
    fclose(target);
}

void download(char title[100])
{
    FILE *fp;
    char desc_path[100] = "/home/kali/modul3/soal2/Client";
    strcat(desc_path, "/");
    strcat(desc_path, title);
    makeDir(desc_path);
    strcat(desc_path, "/");
    strcat(desc_path, "description.txt");

    char input_path[100] = "/home/kali/modul3/soal2/Client";
    strcat(input_path, "/");
    strcat(input_path, title);
    makeDir(input_path);
    strcat(input_path, "/");
    strcat(input_path, "input.txt");

    char server_desc[100] = "/home/kali/modul3/soal2/Server";
    strcat(server_desc, "/");
    strcat(server_desc, title);
    strcat(server_desc, "/");
    strcat(server_desc, "description.txt");
    // printf(desc_path);
    makeFile(server_desc, desc_path);

    char server_input[100] = "/home/kali/modul3/soal2/Server";
    // strcpy(server_path, title);
    // strcat(server_path, "/");
    // strcat(server_path, "input.txt");
    strcat(server_desc, "/");
    strcat(server_desc, title);
    strcat(server_desc, "/");
    strcat(server_desc, "input.txt");
    // printf(input_path);
    makeFile(server_input, input_path);
}

int main(int argc, char const *argv[])
{
    int server_fd, new_socket;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};

    // message for client
    // starting point
    char *start = "register/login? ";
    char *mintaUser = "\nusername: ";
    char *mintaPw = "password: ";

    // error exist
    char *usrExist = "username exist";
    char *passInvalid = "password invalid";
    char *errLogin = "invalid username/password";

    // command
    char *cmd = "add/see/download/submit: ";

    // add
    char *mintaTitle = "Judul problem: ";
    char *mintaDesc = "Filepath description.txt: ";
    char *mintaInput = "Filepath input.txt: ";
    char *mintaOutput = "Filepath output.txt: ";

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0)
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0)
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0)
    {
        perror("accept");
        exit(EXIT_FAILURE);
    }

    FILE *usr, *prob;
    usr = fopen("user.txt", "a");
    fclose(usr);
    prob = fopen("problem.tsv", "a");
    fclose(prob);
    while (1)
    {
        send(new_socket, start, strlen(start), 0);
        memset(buffer, 0, 1024);
        read(new_socket, buffer, 1024);

        if (strcmp(buffer, "register") == 0)
        {
            char usr[100], pw[100];
            memset(buffer, 0, 1024);
            send(new_socket, mintaUser, strlen(mintaUser), 0);
            read(new_socket, buffer, 1024);
            strcpy(usr, buffer);
            int statusUsr;
            statusUsr = checkUsr(usr);

            if (statusUsr == -1)
            {
                send(new_socket, usrExist, strlen(usrExist), 0);
                exit(EXIT_FAILURE);
            }

            memset(buffer, 0, 1024);
            send(new_socket, mintaPw, strlen(mintaPw), 0);
            read(new_socket, buffer, 1024);
            strcpy(pw, buffer);
            int statusPw;
            statusPw = checkPw(pw);

            if (statusPw == -1)
            {
                send(new_socket, passInvalid, strlen(passInvalid), 0);
                exit(EXIT_FAILURE);
            }

            reg(usr, pw);
        }
        if (strcmp(buffer, "login") == 0)
        {
            while (1)
            {
                char user[100], pass[100];
                memset(buffer, 0, 1024);
                send(new_socket, mintaUser, strlen(mintaUser), 0);
                read(new_socket, buffer, 1024);
                strcpy(user, buffer);

                memset(buffer, 0, 1024);
                send(new_socket, mintaPw, strlen(mintaPw), 0);
                read(new_socket, buffer, 1024);
                strcpy(pass, buffer);

                int status = login(user, pass);

                if (status == -1)
                {
                    memset(buffer, 0, 1024);
                    send(new_socket, errLogin, strlen(errLogin), 0);
                }
                else if (status == 1)
                {
                    while (1)
                    {
                        char command[10];
                        memset(buffer, 0, 1024);
                        send(new_socket, cmd, strlen(cmd), 0);
                        read(new_socket, buffer, 1024);
                        strcpy(command, buffer);

                        char *cmd = strtok(command, " ");

                        if (strcmp(cmd, "add") == 0)
                        {
                            char title[100], desc[100], input[100], output[100];

                            memset(buffer, 0, 1024);
                            send(new_socket, mintaTitle, strlen(mintaTitle), 0);
                            read(new_socket, buffer, 1024);
                            strcpy(title, buffer);

                            memset(buffer, 0, 1024);
                            send(new_socket, mintaDesc, strlen(mintaDesc), 0);
                            read(new_socket, buffer, 1024);
                            strcpy(desc, buffer);

                            memset(buffer, 0, 1024);
                            send(new_socket, mintaInput, strlen(mintaInput), 0);
                            read(new_socket, buffer, 1024);
                            strcpy(input, buffer);

                            memset(buffer, 0, 1024);
                            send(new_socket, mintaOutput, strlen(mintaOutput), 0);
                            read(new_socket, buffer, 1024);
                            strcpy(output, buffer);

                            makeDir(title);

                            char descPath[100], inputPath[100], outputPath[100];

                            strcpy(descPath, title);
                            strcat(descPath, "/description.txt");
                            makeFile(desc, descPath);

                            strcpy(inputPath, title);
                            strcat(inputPath, "/input.txt");
                            makeFile(input, inputPath);

                            strcpy(outputPath, title);
                            strcat(outputPath, "/output.txt");
                            makeFile(output, outputPath);

                            addTitle(title, user);
                        }
                        else if (strcmp(cmd, "see") == 0)
                        {
                            char prob[100] = "problem.tsv";
                            FILE *file = fopen(prob, "r");

                            char line[1000];

                            while (fgets(line, sizeof(line), file))
                            {
                                const char sep[10] = "\t";
                                char *token;
                                char title[100], author[100], content[100];

                                token = strtok(line, sep);
                                strcpy(title, token);

                                token = strtok(NULL, sep);
                                strcpy(author, token);

                                strcpy(content, title);
                                strcat(content, " by ");
                                strcat(content, author);

                                send(new_socket, content, strlen(content), 0);
                            }
                            fclose(file);
                        }
                        else if (strcmp(cmd, "download") == 0)
                        {
                            cmd = strtok(NULL, " ");
                            char title[100];
                            strcpy(title, cmd);
                            download(title);
                        }
                    }
                }
            }
        }
    }
    return 0;
}

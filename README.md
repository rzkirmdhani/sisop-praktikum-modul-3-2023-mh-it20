# sisop-praktikum-modul-3-2023-MH-IT20
Laporan pengerjaan soal shift modul 3 Praktikum Sistem Operasi 2023 Kelompok IT20

## Anggota Kelompok:
| N0. | Nama Anggota | NRP |
|-----| ----------- | ----------- |
| 1. | M. Januar Eko Wicaksono | 5027221006 |
| 2. | Rizki Ramadhani | 5027221013 |
| 3. |Khansa Adia Rahma | 5027221071 | 

## Soal 1

### Study case soal 1
---
Epul memiliki seorang adik SMA bernama Azka. Azka bersekolah di Thursina Malang. Di sekolah, Azka diperkenalkan oleh adanya kata matriks. Karena Azka baru pertama kali mendengar kata matriks, Azka meminta tolong kepada kakaknya Epul untuk belajar matriks.  Karena Epul adalah seorang kakak yang baik, Epul memiliki ide untuk membuat program yang bisa membantu adik tercintanya, tetapi dia masih bingung untuk membuatnya. Karena Epul adalah bagian dari IT05 dan praktikan sisop, sebagai warga IT05 yang baik, bantu Epul mengerjakan programnya dengan ketentuan sebagai berikut :

### Problem
---
a Membuat program C dengan nama belajar.c, yang berisi program untuk melakukan perkalian matriks. Agar ukuran matriks bervariasi, maka ukuran matriks pertama adalah [nomor_kelompok]×2 dan matriks kedua 2×[nomor_kelompok]. Isi dari matriks didefinisikan di dalam code. Matriks nantinya akan berisi angka random dengan rentang pada matriks pertama adalah 1-4 (inklusif), dan rentang pada matriks kedua adalah 1-5 (inklusif). Tampilkan matriks hasil perkalian tadi ke layar.

b. Karena Epul merasa ada yang kurang, Epul meminta agar hasil dari perkalian tersebut dikurang 1 di setiap matriks.

c. Karena Epul baru belajar modul 3, Epul ngide ingin meminta agar menerapkan konsep shared memory. Buatlah program C kedua dengan nama yang.c. Program ini akan mengambil variabel hasil pengurangan dari perkalian matriks dari program belajar.c (program sebelumnya). Hasil dari pengurangan perkalian matriks tersebut dilakukan transpose matriks dan diperlihatkan hasilnya. 
(Catatan: wajib menerapkan konsep shared memory)

d. Setelah ditampilkan, Epul pengen adiknya belajar lebih, sehingga untuk setiap angka dari transpose matriks tersebut, carilah nilai faktorialnya. Tampilkan hasilnya ke layar dengan format seperti matriks.

**Contoh:** 

matriks: 
```c
1	2  	3

2	2	4
```
maka: 
```c
1	4  	6

4	4	24
```
(Catatan: Wajib menerapkan thread dan multithreading dalam penghitungan faktorial).

e. Epul penasaran mengapa dibuat thread dan multithreading pada program sebelumnya, dengan demikian dibuatlah program C ketiga dengan nama rajin.c. Pada program ini, lakukan apa yang telah kamu lakukan seperti pada yang.c namun tanpa menggunakan thread dan multithreading. Buat dan modifikasi sedemikian rupa sehingga kamu bisa menunjukkan perbedaan atau perbandingan (hasil dan performa) antara program dengan multithread dengan yang tidak. 
Dokumentasikan dan sampaikan saat demo dan laporan resmi.

Catatan:
- Key memory diambil berdasarkan program dari belajar.c sehingga program belajar.c tidak perlu menghasilkan key memory (value). Dengan demikian, pada program yang.c dan rajin.c hanya perlu mengambil key memory dari belajar.c
- Untuk kelompok 1 - 9, 11 - 19, 21 menggunakan  digit akhir saja.
	Contoh : 19 -> 9
- Untuk kelompok 10 dan 20 menambahkan digit awal dan akhir.
			Contoh : 20 - > 2

### Solution
---
[Source Code](./soal_1)

untuk membuat direktori baru yang akan menyimpan file playlist.csv dan file playlist_keren.sh

```c
mkdir playlist
```
Fungsi dari function ini adalah untuk membuat suatu directory. Sebelum menjalankan command `mkdir`, akan dicek terlebih dahulu apakah path dari directory yang akan dibuat sudah ada atau belum. Bila sudah ada, maka command `mkdir` tidak akan dijalankan.

Command `mkdir` akan membuat directory sesuai dengan path yang dioper melalui parameter `dir`.
Kemudian unduh playlist.csv dan letakkan ke dalam folder playlist yang sudah dibuat pada.Meletakkan kedua file di folder yang sama, agar berhasil saat dieksekusi.  
- Langkah 1.
```c
nano playlist.sh
```
Perintah diatas Untuk membuat file shebang baru, dan mengedit isi di dalamnya.

- Source Code nano playlist.sh
```c
source code
```

### Hasil
---

1. Isi dari File `playlist_keren.sh`

![Isi File 'playlist_keren.sh'](img/Soal_1/Isi_playlist_keren.sh.png)

2. Isi dari File Hasil dijalankannya `playlist_keren.sh`
![Hasil Program](img/Soal_1/Results_playlist_keren.sh.png)

### Kendala
---
1. Tidak ada kendala apapun.

### Revisi
---
> Program yang ditambahkan setelah soal shift dilakukan

1. Tidak ada, pada saat demo sudah lancar dan tidak ada tambahan dari aslab penguji


## Soal 2

### Study case soal 2
---
Messi the only GOAT sedang gabut karena Inter Miami Gagal masuk Playoff. Messi menghabiskan waktu dengan mendengar lagu. Karena gabut yang tergabut gabut, Messi penasaran berapa banyak jumlah kata tertentu yang muncul pada lagu tersebut. Messi mencoba ngoding untuk mencari jawaban dari kegabutannya. Pertama-tama dia mengumpulkan beberapa lirik lagu favorit yang disimpan di file. Beberapa saat setelah mencoba ternyata ngoding tidak semudah itu, Messipun meminta tolong kepada Ronaldo, tetapi karena Ronaldo sibuk main di liga oli, Ronaldopun meminta tolong kepadamu untuk dibuatkan kode dari program Messi dengan ketentuan sebagai berikut : 

(Wajib menerapkan konsep pipes dan fork seperti yang dijelaskan di modul Sisop. Gunakan 2 pipes dengan diagram seperti di modul 3).
### Problem
---
a. Shinichi akan melakukan register menggunakan email, username, serta password. Username yang dibuat bebas, namun email bersifat unique.

b. Shinichi khawatir suatu saat nanti Ran akan curiga padanya dan dapat mengetahui password yang ia buat. Maka dari itu, Shinichi ingin membuat password dengan tingkat keamanan yang tinggi.

- Password tersebut harus di encrypt menggunakan base64
- Password yang dibuat harus lebih dari 8 karakter
- Harus terdapat paling sedikit 1 huruf kapital dan 1 huruf kecil
- Password tidak boleh sama dengan username
- Harus terdapat paling sedikit 1 angka 
- Harus terdapat paling sedikit 1 simbol unik. 
c. Karena Shinichi akan membuat banyak akun baru, ia berniat untuk menyimpan seluruh data register yang ia lakukan ke dalam folder users file users.txt. Di dalam file tersebut, terdapat catatan seluruh email, username, dan password yang telah ia buat.

d. Shinichi juga ingin program register yang ia buat akan memunculkan respon setiap kali ia melakukan register. Respon ini akan menampilkan apakah register yang dilakukan Shinichi berhasil atau gagal

e. Setelah melakukan register, Shinichi akan langsung melakukan login untuk memastikan bahwa ia telah berhasil membuat akun baru. Login hanya perlu dilakukan menggunakan email dan password.

f. Ketika login berhasil ataupun gagal, program akan memunculkan respon di mana respon tersebut harus mengandung username dari email yang telah didaftarkan.
- Ex: 
    - LOGIN SUCCESS - Welcome, [username]
    - LOGIN FAILED - email [email] not registered, please register first

g. Shinichi juga mencatat seluruh log ke dalam folder users file auth.log, baik login ataupun register.
- Format: [date] [type] [message]
- Type: REGISTER SUCCESS, REGISTER FAILED, LOGIN SUCCESS, LOGIN FAILED
- Ex:
    - [23/09/17 13:18:02] [REGISTER SUCCESS] user [username] registered successfully
    - [23/09/17 13:22:41] [LOGIN FAILED] ERROR Failed login attempt on user with email [email]

### Solution
---
[Source Code](./soal_2/)

untuk membuat direktori baru yang akan menyimpan file playlist.csv dan file playlist_keren.sh

```c
mkdir register_login_project
cd register_login_project
```
Fungsi dari function ini adalah untuk membuat suatu directory. Sebelum menjalankan command `mkdir`, akan dicek terlebih dahulu apakah path dari directory yang akan dibuat sudah ada atau belum. Bila sudah ada, maka command `mkdir` tidak akan dijalankan.

Command `mkdir` akan membuat directory sesuai dengan path yang dioper melalui parameter `dir`. Membuat folder kerja baru

- Membuat skrip register
 
```c
touch register.sh
```
- Membuka file register.sh menggunakan nano sebagai teks editor.

```c
nano register.sh 
```

- Source Code 'register.sh'

```c
#!/bin/bash

# Meminta input dari pengguna
read -p "Masukkan email: " email
read -p "Masukkan username: " username
read -s -p "Masukkan password: " password
echo

# Memeriksa apakah email sudah terdaftar
if grep -q "$email" users.txt; then
  echo "Email sudah terdaftar. Silakan gunakan email lain."
else
  # Menyimpan informasi ke dalam file users.txt
  echo "$email:$username:$password" >> users.txt
  echo "Registrasi berhasil!"
fi

```

- Membuat file ‘user.txt’ untuk menyimpan informasi pengguna yang sudah terdaftar.

```c
touch users.txt
```

- Mengizinkan eksekusi pada skrip register.

```c
chmod +x register.sh
```

- Menjalankan registrasi

```c
./register.sh
```
- Membuka file login.sh menggunakan nano sebagai teks editor.

```c
nano login.sh
```
- Source Code 'login.sh'

```c
#!/bin/bash

# Meminta input dari pengguna
read -p "Masukkan email: " email
read -s -p "Masukkan password: " password
echo

# Cek apakah email sudah terdaftar
if grep -q "$email" users.txt; then
  # Ambil password terenkripsi dari file
  encrypted_password=$(grep "$email" users.txt | awk '{print $3}')
  
  # Dekripsi password dan cocokkan
  decrypted_password=$(echo -n "$encrypted_password" | base64 -d)
  if [ "$decrypted_password" = "$password" ]; then
    username=$(grep "$email" users.txt | awk '{print $2}')
    echo "LOGIN SUCCESS - Welcome, $username"
    log="[LOGIN] [$(date +'%d/%m/%y %H:%M:%S')] [LOGIN SUCCESS] user $username logged in successfully"
  else
    echo "LOGIN FAILED - Password salah."
    log="[LOGIN] [$(date +'%d/%m/%y %H:%M:%S')] [LOGIN FAILED] ERROR Failed login attempt on user with email $email"
  fi
else
  echo "LOGIN FAILED - email $email not registered, please register first"
  log="[LOGIN] [$(date +'%d/%m/%y %H:%M:%S')] [LOGIN FAILED] ERROR Failed login attempt on user with email $email"
fi

# Catat log
echo "$log" >> auth.log

```
- Membuat file user.txt dan auth.log kosong.

```c
touch users.txt
touch auth.log
```
- Memberikan izin eksekusi ke kedua skrip

```c
chmod +x register.sh
chmod +x login.sh
```

- Untuk registrasi

```c
./register.sh
```
- Untuk Login
```c
./login.sh
```

### Hasil
---
1. Isi dari File register.sh

![Isi register](img/Soal_2/register.sh.jpeg)

2. Isi dari File login.sh 

![Isi Login](img/Soal_2/login.sh.jpeg)


### Kendala
---
1. Error tidak bisa login
![Tidak bisa login](img/Soal_2/error_tidak_bisa_login.jpeg)

### Revisi
---
> Program yang ditambahkan setelah soal shift dilakukan

1. Tambahan untuk dibenerin lagi dibagian loginnya karena eror waktu demo 

## Soal 3

### Study case soal 3
---
Aether adalah seorang gamer yang sangat menyukai bermain game Genshin Impact. Karena hobinya, dia ingin mengoleksi foto-foto karakter Genshin Impact. Suatu saat Pierro memberikannya sebuah Tautan yang berisi koleksi kumpulan foto karakter dan sebuah clue yang mengarah ke endgame. Ternyata setiap nama file telah dienkripsi dengan menggunakan base64. Karena penasaran dengan apa yang dikatakan piero, Aether tidak menyerah dan mencoba untuk mengembalikan nama file tersebut kembali seperti semula.

### Problem
---
a. Aether membuat script bernama genshin.sh, untuk melakukan unzip terhadap file yang telah diunduh dan decode setiap nama file yang terenkripsi dengan base64 . Karena pada file list_character.csv terdapat data lengkap karakter, Aether ingin merename setiap file berdasarkan file tersebut. Agar semakin rapi, Aether mengumpulkan setiap file ke dalam folder berdasarkan region tiap karakter
- Format: Nama - Region - Elemen - Senjata.jpg

b. Karena tidak mengetahui jumlah pengguna dari tiap senjata yang ada di folder "genshin_character".Aether berniat untuk menghitung serta menampilkan jumlah pengguna untuk setiap senjata yang ada
- Format: [Nama Senjata] : [total]

Untuk menghemat penyimpanan. Aether menghapus file - file yang tidak ia gunakan, yaitu genshin_character.zip, list_character.csv, dan genshin.zip

c. Namun sampai titik ini Aether masih belum menemukan clue endgame yang disinggung oleh Pierro. Dia berfikir keras untuk menemukan pesan tersembunyi tersebut. Aether membuat script baru bernama find_me.sh untuk melakukan pengecekan terhadap setiap file tiap 1 detik. Pengecekan dilakukan dengan cara meng-ekstrak tiap gambar dengan menggunakan command steghide. Dalam setiap gambar tersebut, terdapat sebuah file txt yang berisi string. Aether kemudian mulai melakukan dekripsi dengan base64 pada tiap file txt dan mendapatkan sebuah url. Setelah mendapatkan url yang ia cari, Aether akan langsung menghentikan program find_me.sh serta mendownload file berdasarkan url yang didapatkan.

d. Dalam prosesnya, setiap kali Aether melakukan ekstraksi dan ternyata hasil ekstraksi bukan yang ia inginkan, maka ia akan langsung menghapus file txt tersebut. Namun, jika itu merupakan file txt yang dicari, maka ia akan menyimpan hasil dekripsi-nya bukan hasil ekstraksi. Selain itu juga, Aether melakukan pencatatan log pada file image.log untuk setiap pengecekan gambar
- Format: [date] [type] [image_path]
- Ex: 
    - [23/09/11 17:57:51] [NOT FOUND] [image_path]
    - [23/09/11 17:57:52] [FOUND] [image_path]

e. Hasil akhir:
= genshin_character
- find_me.sh
- genshin.sh
- image.log
- [filename].txt
- [image].jpg

### Solution 
---
[Source Code](./soal_3/)

- Pertama-tama kita akan mendownload file gdrive yang diperintahkan dengan cara:

```c
wget --no-check-certificate "https://drive.google.com/uc?id=1RBuZBtK6q-_I7S_WgeioqIRrUlvRRbq2" -O "genshin.zip"

```

- Kemudian kita buat file bash 'genshin.sh' untuk dimasukkan source code seperti dibawah ini untuk meng-solve pertanyaan a dan b:

```c
genshin_zip="genshin"
genshin_character_zip="genshin_character.zip"
list_character="list_character.csv"

unzip "$genshin_zip"

  if [ $? -eq 0 ]; then
        echo "Unzip '$genshin_zip' berhasil."
  else
        echo "Error: Gagal unzip '$genshin_zip'."
        exit 1
  fi

  if [ ! -f "$genshin_character_zip" ]; then
         echo "Error: File '$genshin_character_zip' tidak ada."
        exit 1
  fi

unzip "$genshin_character_zip"

  if [ $? -eq 0 ]; then
         echo "Unzip '$genshin_character_zip' berhasil."
  else
         echo "Error: Gagal unzip '$genshin_character_zip'."
        exit 1
  fi

mkdir -p genshin_character

for encoded_filename in *.jpg; do
    decoded_filename=$(echo "$encoded_filename" | base64 -d 2>/dev/null)
    cocoklogi=$(grep -F "$decoded_filename" "$list_character")

    if [ -z "$cocoklogi" ]; then
        echo "Error: Tidak ada baris yang cocok untuk '$decoded_filename' dalam '$list_character'."
        exit 1
    fi

    nama=$(echo "$cocoklogi" | awk -F',' '{gsub("\r",""); print $1}')
    region=$(echo "$cocoklogi" | awk -F',' '{gsub("\r",""); print $2}')
    elemen=$(echo "$cocoklogi" | awk -F',' '{gsub("\r",""); print $3}')
    senjata=$(echo "$cocoklogi" | awk -F',' '{gsub("\r",""); print $4}')

    mkdir -p "genshin_character/$region"

    namacharacter="${nama}-${region}-${elemen}-${senjata}"
    mv "$encoded_filename" "genshin_character/$region/${namacharacter}.jpg"
    echo "Merename: $encoded_filename -> genshin_character/$region/${namacharacter}.jpg"
for weapon in Catalyst Sword Claymore Bow Polearm; do
        count=$(find genshin_character/ -name "*$weapon.jpg" | wc -l)
        echo "nama senjata $weapon : $count"
done

rm genshin_character.zip
rm genshin.zip
rm list_character.csv
```

- Setelah itu save dan jalankan 'genshin.sh' dengan command dibawah ini:


```c
chmod +x genshin.sh
./genshin.sh

```

- Kemudian buat file bash 'find_me.sh' untuk mencari url dan gambar yang tersembunyi. dengan cara:

```c
nano find_me.sh

```
- Isi source code dari 'find_me.sh adalah:

```c
!/bin/bash
set -x

regions=("Mondstat" "Liyue" "Inazuma" "Sumeru" "Fontaine")
status=false

for region in "${regions[@]}"; do
  cd "genshin/genshin_character/$region"
  for file in *; do
    time=$(date +"%Y/%m/%d %H:%M:%S")
    image_path="./$file"
    if [ -f "$file" ]; then
      steghide extract -sf "$file" -p ""
      name=$(awk -F' -' '{print $1}' <<< "$file")
      if [ -f "$name.txt" ]; then
        cat "$name.txt" | base64 -d > "./retracted.txt"
        status=true
      fi
    fi

    url_valid='https?://\S+'
    if [[ $status = true && $url =~ $url_valid ]]; then
      wget "$url"
      echo "[$time] [true] [$image_path]" >> "./image.log"
      break
    else
      echo "[$time] [false] [$image_path]" >> "./image.log"
    fi

    rm -f "$name.txt"
    sleep 1ue && $url =
  done
  if [ $status = true ]; then
    break
  fi
  cd - > /dev/null
done

```

- - Setelah itu save dan jalankan 'find_me.sh' dengan command dibawah ini:


```c
for region in genshin_character/*; do
    if [ -d "$region" ]; then
        for image in "$region"/*; do
            image_filename=$(echo "$image" | awk -F ' -' '{print $1}')
            image_basename=$(echo "$image_filename" | awk -F '/' '{print $3}')
            steghide extract -sf "$image" -p "" -xf "${image_basename}.txt"
            decoded_message=$(cat "${image_basename}.txt" | base64 --decode)
            timestamp=$(date '+%d/%m/%y %H:%M:%S')

if [[ "$decoded_message" == http ]]; then
    wget "$decoded_message"
    echo "[$timestamp] [FOUND] [$image]" >> image.log
    pkill -f "find_me.sh"
else
    echo "[$timestamp] [NOT FOUND] [$image]" >> image.log
    rm "${image_basename}.txt"
    fi
        sleep 1
done
    fi
done

```

- Setelah itu save dan jalankan 'find_me.sh' dengan command dibawah ini:

```c
chmod +x find_me.sh
./find_me.sh

```
maka akan mendeteksi satu-persatu dari file .jpg di dalam folder region tersebut untuk menemukan file yang tersembunyi.

- Setelah selesai dideteksi maka akan muncul file 'image.log' dan dibuka untuk melihat file tersebut sudah ditemukan apa belum. Dengan cara:


```c
cat image.log

```
Setelah ditemukan di dalam file 'image.log' ter-solve masalah file yang tersembunyi tersebut.


### Hasil
---

1. Results dari senjata

![Results Senjata](img/Soal_3/Result_Senjata.png)

2. Results Found File tersembunyi

![Results Found](img/Soal_3/File_Found.png)

### Kendala
---
1. Eror Folder tidak ditemukan

![Folder Tidak ditermukan](img/Soal_3/Error_folder_tidak_ditemukan.png)

2. Error File Not Found
![File Tidak ditermukan](img/Soal_3/Error_tidak_bisa_menemukan_file.png)


### Revisi
---
> Program yang ditambahkan setelah soal shift dilakukan

1. Diberitahu code untuk mencari url dan file yang tersembunyi, yang sebelumnya tidak bisa, setelah revisi menjadi ditemukan.


## Soal 4

### Study case soal 4
---
Defatra sangat tergila-gila dengan laptop legion nya. Suatu hari, laptop legion nya mendadak rusak 🙁 Tentu saja, Defatra adalah programer yang harus 24/7 ngoding tanpa menggunakan vscode di laptop legion nya.  Akhirnya, dia membawa laptop legion nya ke tukang servis untuk diperbaiki. Setelah selesai diperbaiki, ternyata biaya perbaikan sangat mahal sehingga dia harus menggunakan uang hasil slot nya untuk membayarnya. Menurut Kang Servis, masalahnya adalah pada laptop Defatra yang overload sehingga mengakibatkan crash pada laptop legion nya. Untuk mencegah masalah serupa di masa depan, Defatra meminta kamu untuk membuat program monitoring resource yang tersedia pada komputer.
Buatlah program monitoring resource pada laptop kalian. Cukup monitoring ram dan monitoring size suatu directory. Untuk ram gunakan command `free -m`. Untuk disk gunakan command `du -sh <target_path>`. Catat semua metrics yang didapatkan dari hasil `free -m`. Untuk hasil `du -sh <target_path>` catat size dari path directory tersebut. Untuk target_path yang akan dimonitor adalah /home/{user}/

### Problem
---
a. Masukkan semua metrics ke dalam suatu file log bernama metrics_{YmdHms}.log. {YmdHms} adalah waktu disaat file script bash kalian dijalankan. Misal dijalankan pada 2023-01-31 15:00:00, maka file log yang akan tergenerate adalah metrics_20230131150000.log.

b. Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap menit. 

c. Kemudian, buat satu script untuk membuat agregasi file log ke satuan jam. Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis. Berikut contoh nama file hasil agregasi metrics_agg_2023013115.log dengan format metrics_agg_{YmdH}.log 

d. Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file.  

- Note:
    - Nama file untuk script per menit adalah minute_log.sh
    - Nama file untuk script agregasi per jam adalah aggregate_minutes_to_hourly_log.sh
    - Semua file log terletak di /home/{user}/log
    - Semua konfigurasi cron dapat ditaruh di file skrip .sh nya masing-masing dalam bentuk comment

- Berikut adalah contoh isi dari file metrics yang dijalankan tiap menit:

```c
mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size 15949,10067,308,588,5573,4974,2047,43,2004,/home/youruser/test/,74M
```

- Berikut adalah contoh isi dari file aggregasi yang dijalankan tiap jam:


```c
type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size minimum,15949,10067,223,588,5339,4626,2047,43,1995,/home/user/test/,50M maximum,15949,10387,308,622,5573,4974,2047,52,2004,/home/user/test/,74M average,15949,10227,265.5,605,5456,4800,2047,47.5,1999.5,/home/user/test/,62M
```


### Solution
---
[Source Code](./soal_4)

untuk membuat direktori baru yang akan menyimpan file 'minute_log.sh' dan 'aggregate_minutes_to_hourly_log' 

```c
mkdir nomor4
```
Fungsi dari function ini adalah untuk membuat suatu directory. Sebelum menjalankan command `mkdir`, akan dicek terlebih dahulu apakah path dari directory yang akan dibuat sudah ada atau belum. Bila sudah ada, maka command `mkdir` tidak akan dijalankan.

Command `mkdir` akan membuat directory sesuai dengan path yang dioper melalui parameter `dir`.

- Pertama-tama buatlah file 'minute_log.sh' dengan cara:
```c
nano minute_log.sh
```

- Source Code nano minute_log.sh
```c
#!/bin/bash

# Log directori
log_dir="/home/yanuar/nomor4"

# Pengaturan waktu yang digunakan
timestamp=$(date +'%Y%m%d%H%M%S')

# Deklarasi Ram dan swap dengan 'free -m'
ram_info=$(free -m | awk 'NR==2{print $2","$3","$4","$5","$6","$7}')
swap_info=$(free -m | awk 'NR==3{print $2","$3","$4}')

# Target path 'du -sh'
target_path="/home/yanuar/"
dir_size_info=$(du -sh "$target_path" | awk '{print $1}')

# Menyimpan Source monitoring
echo "$ram_info,$swap_info,$target_path,$dir_size_info" >> "$log_dir/metrics_$timestamp.log"
```
- Setelah itu set up dulu untuk mengatur waktunya dengan cara:
```c
crontab -e
```
- Isi crontab -e
```c
# Menjalankan skrip per menit setiap menit
* * * * * /home/imamjk/SiSop/minute_log.sh

# Menjalankan skrip agregasi per jam setiap jam
0 * * * * /home/imamjk/SiSop/aggregate_minutes_to_hourly_log.sh 
```

- Kemudian jalankan 'find_me.sh' dengan cara:
```c
chmod +x find_me.sh
./find_me.sh
```
maka akan berjalan dan menghasilkan metrics_(sesuai dengan jam yang dijalankan pada saat itu).

- kemudian untuk membuat aggregasi file dalam satuan jam dilakukan dengan membuat file bash 'aggregate_minutes_to_hourly_log' dengan cara:
```c
nano aggregate_minutes_to_hourly_log.sh
```

- Source Code nano aggregate_minutes_to_hourly_log.sh
```c
#!/bin/bash

# Direktori log
log_dir="/home/yanuar/nomor4"

# Waktu saat ini untuk mengganti agregasi per jam
hourly_timestamp=$(date +'%Y%m%d%H')

# Menggabungkan semua file log per menit dalam satu jam
cat "$log_dir/metrics_$hourly_timestamp"* > "$log_dir/metrics_agg_$hourly_timestamp.log"

# Menghitung nilai minimum, maksimum, dan rata-rata dari metrik RAM dan Disk
min_ram=$(awk -F',' 'NR>1{print $2}' "$log_dir/metrics_agg_$hourly_timestamp.log" | sort -n | head -n 1)
max_ram=$(awk -F',' 'NR>1{print $2}' "$log_dir/metrics_agg_$hourly_timestamp.log" | sort -n | tail -n 1)
avg_ram=$(awk -F',' 'NR>1{sum+=$2}END{print sum/(NR-1)}' "$log_dir/metrics_agg_$hourly_timestamp.log")

min_disk=$(awk -F',' 'NR>1{print $12}' "$log_dir/metrics_agg_$hourly_timestamp.log" | sort -h | head -n 1)
max_disk=$(awk -F',' 'NR>1{print $12}' "$log_dir/metrics_agg_$hourly_timestamp.log" | sort -h | tail -n 1)
avg_disk=$(awk -F',' 'NR>1{sum+=$12}END{print sum/(NR-1)}' "$log_dir/metrics_agg_$hourly_timestamp.log")

# Simpan hasil agregasi dalam satu file
echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_cache,mem_available,swap_total,swap_used,swap_free,swap_cache,path,path_size" > "$log_dir/metrics_agg_$hourly_timestamp.log"
echo "minimum,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_disk" >> "$log_dir/metrics_agg_$hourly_timestamp.log"
echo "maximum,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_disk" >> "$log_dir/metrics_agg_$hourly_timestamp.log"
echo "average,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_disk" >> "$log_dir/metrics_agg_$hourly_timestamp.log"
```

- Kemudian jalankan 'aggregate_minutes_to_hourly_log.sh' dengan cara:
```c
chmod +x aggregate_minutes_to_hourly_log.sh
./aggregate_minutes_to_hourly_log.sh
```
maka akan mengganti file namanya mejadi metrics_agg_(sesuai jam yang dilakukan pada saat run).

- Kemudian Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file. Dengan cara:

```c
chmod 600 metrics_*.aggregate_minutes_to_hourly_log,sh*.log 
```
maka file log tersebut hanya bisa dibaca oleh user pemilik file.

### Hasil
---

1. Results metrics keduanya
![Results Metric keduanya](img/Soal_4/Results_metrcis.png)

### Kendala
---
1. Error ada newline di line 13.
![Error Line 13](img/Soal_4/Error_agg.png)

### Revisi
---
> Program yang ditambahkan setelah soal shift dilakukan

1. Pada saat demo terdapat kendala pada saat menjal
aggregate_minutes_to_hourly_log.sh jadi harus dibenerin lagi dan udah bisa


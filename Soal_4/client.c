#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#define PORT 15050
#define BUFFER_SIZE 1024

int main() {
    int sockfd;
    struct sockaddr_in server_addr;
    char buffer[BUFFER_SIZE];
    FILE *fp;

    // Create socket
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0) {
        perror("Socket creation failed");
        exit(1);
    }

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(PORT);
    server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");

    printf("Masukkan nama file yang akan diterima: ");
    scanf("%s", buffer);

    sendto(sockfd, buffer, strlen(buffer), 0, (struct sockaddr *)&server_addr, sizeof(server_addr));

    fp = fopen(buffer, "wb");
    if (fp == NULL) {
        perror("File open failed");
        exit(1);
    }

    while (1) {
        size_t n = recvfrom(sockfd, buffer, BUFFER_SIZE, 0, NULL, NULL);
        if (n <= 0) {
            break;
        }
        fwrite(buffer, 1, n, fp);
    }

    fclose(fp);
    close(sockfd);

    printf("File berhasil diterima.\n");

    return 0;
}
